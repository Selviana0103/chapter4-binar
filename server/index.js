import http from "http";
import fs from "fs";

const HOST = "localhost";
const PORT = 8085;

http.createServer((req, res) => {
    switch (req.url) {
        case "/":
            req.url = "challenge4.html";
            break;
        case "/cars":
            req.url = "carimobil.html";
            break;
    }
    let path = "public/" + req.url;
    fs.readFile(path, (err, data) => {
        res.writeHead(200);
        res.end(data);
    });
})
.listen(PORT, HOST, () => {
    console.log(`Server already listening on http://${HOST}:${PORT}`);
});